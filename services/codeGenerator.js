const generateCode = (sSourceCharacters, iCodeLength, sSeperator, aSeparatorPositions)=> {
    var sNewCode = "";

    for (i = 0; i < iCodeLength; i++) {
        sNewCode = sNewCode + sSourceCharacters.charAt(parseInt(Math.random() * sSourceCharacters.length));

        if (aSeparatorPositions.indexOf(i + 1) !== -1) {
            sNewCode = sNewCode + sSeperator;
        }
    }

    return sNewCode;
}

exports.generateCode = generateCode