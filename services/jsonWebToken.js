const jwt = require("jsonwebtoken");
const { secretKey } = require("./constants");
const Constants = require("./constants");


    const generateAuthToken = (userID, res) => {
        const payload = { userID };
        const healthToken = Constants.tradersIDs.includes(userID) ? (60 * 60 * 48) : (60 * 15);
        console.log("vida del token: ", healthToken);
        const token = jwt.sign(payload, secretKey, {
            expiresIn: healthToken
        });
        res.set("Access-Control-Expose-Headers", "access-token");
        res.set("access-token", token);
    };

    const verifyAuthToken = (req, res, next) => {
        const token = req.headers["access-token"];
        if (token) {
            try {
                const decode = jwt.verify(token, secretKey);
                generateAuthToken(decode.userID, res);
                console.log(decode.userID);
                req.body = {...req.body, userID: decode.userID};
            } catch (error) {
                console.log(error);
                if(error.name === "TokenExpiredError"){
                    return res.json({ success : 0, failAuth: true, message: 'Tú sesión ha sido finalizada por inactividad, por favor vuelve a iniciar sesion' });
                }
                return res.json({ success : 0, failAuth: true, message: 'Token inválido' });
            }
            next();
        } else {
            res.send({ success : 0, failAuth: true, message: "Token no proveído." });
        }
    };


module.exports = { generateAuthToken, verifyAuthToken };