const Binance = require("../forks/binance-api-node").default
const BinanceDos = require('node-binance-api');
const Api = require("../models/Api")
const Constants = require("../services/constants")
const {fixedToPattern} = require("../services/numbers")
const ProtectedBalance = require("../models/ProtectedBalance")

class Exchange {
    async getClient(userID){
        let credentials = await Api.findOne({userID})
        if(!credentials) throw "No se encuentran API´s de Binance registradas en la base de datos"
        return Binance({
            apiKey: credentials.apiKey,
            apiSecret: credentials.apiSecret
        })
    }
    async getAlternativeClient(userID){
        let credentials = await Api.findOne({userID})
        if(!credentials) throw "No se encuentran API´s de Binance registradas en la base de datos"
        return new BinanceDos().options({
            APIKEY: credentials.apiKey,
            APISECRET: credentials.apiSecret
        });
    }
    //funciones auxiliares
    //integrar balances protegidos
    async getCoinBalance(userID, coin, _client){
        try {
            //si no enviamos conexion genera una nueva
            const client = _client ? _client : await this.getClient(userID)
            let accntInf = await client.accountInfo({'recvWindow': 50000})
            let filt = accntInf.balances.filter(val=> val.asset === coin)
            return {success: 1, balance: filt[0].free, allBalances: accntInf.balances}
        } catch (error) {
            return {success: 0, message: error.message ? error.message : error}
        }
    }
    async getStakeholdersCoinsBalance(userID, coin, _client){
        try {
            //si no enviamos conexion genera una nueva
            const client = _client ? _client : await this.getClient(userID)
            let accntInf = await client.accountInfo({'recvWindow': 50000})
            let filt = accntInf.balances.filter(val=> val.asset === "BTC")
            let filtr2 = accntInf.balances.filter(val=> val.asset === coin)
            return {success: 1, balance: parseFloat(filt[0].free), altcoinBalance: parseFloat(filtr2[0].free),  allBalances: accntInf.balances}
        } catch (error) {
            return {success: 0, message: error.message ? error.message : error}
        }
    }
    //inclluir despues el porcentaje del balance total
    async getBuyBalance(symbol, lotSize, binanceClient, pretendedToBuy) {
        let stats = await binanceClient.dailyStats({ symbol })
        let quantityToBuy = fixedToPattern(newBalance / parseFloat(stats.askPrice), lotSize)
        return quantityToBuy
    }
    async getProtected(userID, coin){
        let protectedAmounts = await ProtectedBalance.findOne({userID})
        if(!protectedAmounts) return 0
        console.log("si tiene protected de las moneda ", protectedAmounts)
        const { balances } = protectedAmounts
        let matchs = balances.filter(val=> val.asset === coin)
        if(matchs.length === 0 ) return 0
        return parseFloat(matchs[0].quantity)
    }
    async createBuyOrder(userID, symbol, price, lotSize, pretendedToBuy){
        try {
            const binanceClient = await this.getClient(userID)
            //limpieza del precio y sacar cant a comprar en el asset destino
            let buyBalance = fixedToPattern(parseFloat(pretendedToBuy) / parseFloat(price), lotSize)
            let cleanPrice = this.convertExponentialToDecimal(price)
            let responseBuyOrder = await binanceClient.order({ symbol, side: "BUY", quantity: buyBalance, price: cleanPrice })    
            console.log("la respuesta de buy order", responseBuyOrder)
            return { success: 1, orderID: responseBuyOrder.clientOrderId, newOrder: responseBuyOrder, buyBalance }
        } catch (error) {
            console.log("ocurrio un error", error)
            return { success: 0, message: error.message ? error.message : error }
        }
    }
    async deleteOCO( userID, symbol ){
        try {
            const binanceClient = await this.getClient(userID)
            let response = await binanceClient.cancelOpenOrders({ symbol })
            console.log("response de cancelar oco", response)
            return {success: 1}
        } catch (error) {
            return { success: 0, message: error.message ? error.message : error }
        }
    }
    async newOCO(userID, tradedPair, lotSize, symbol, _sl, _gain, beforeQuantity){
        try {
            const binanceClient = await this.getClient(userID)
            const { success, balance } = await this.getCoinBalance(userID, tradedPair, binanceClient)
            if ( success === 0 ) throw "No se ha podido obtener el balance por problemas con las API."
            let notosell = await this.getProtected(userID, tradedPair)
            console.log("la cantidad a vender es ", balance, "y lo protegido es ",notosell)
            let netBalance = balance - notosell
            const quantityToSell = fixedToPattern(netBalance, lotSize)
            if(quantityToSell > 0){
                //asignamos balance a orden
                let cleanSL = this.convertExponentialToDecimal(_sl)
                let cleanGain = this.convertExponentialToDecimal(_gain)
                let respNewOCO = await binanceClient.orderOco({ symbol, side:  "SELL", quantity: quantityToSell, price: cleanGain, stopPrice: cleanSL, stopLimitPrice: cleanSL, 'recvWindow': 50000 })
                return { success: 1, orderID: respNewOCO.orders[0].clientOrderId }
            } else {
                throw "La orden de venta ya ha sido generada anteriormente."
            }
        } catch (error) {
            console.log("hub", error)
            return { success: 0, message: error.message ? error.message : error }
        }
    }
    async getOrder(orderID, userID, symbol){
        try {
            const binanceClient = await this.getClient(userID)
            let order = await binanceClient.getOrder({ symbol, origClientOrderId: orderID })    
            return {success: 1, order}
        } catch (error) {
            return { success: 0, message: error.message ? error.message : error }
        }
    }
    async editBuyOrder(userID, symbol, quantity, enterprice, lotSize){
        try {
            const binanceClient = await this.getClient(userID)
            let buyBalance = fixedToPattern(parseFloat(quantity), lotSize)
            let cleanPrice = this.convertExponentialToDecimal(enterprice)

            let order = await binanceClient.order({ symbol, side: "BUY", quantity : buyBalance, price: cleanPrice })  
            console.log("repsuesta de crear buy order", order)
            return {success: 1, order}
        } catch (error) {
            return { success: 0, message: error.message ? error.message : error }
        }
    }
    async panicSell(userID, inPair, lotSize, symbol){
        try {
            console.log("nuevo panic sell", userID, inPair, lotSize, symbol)
            const binanceClient = await this.getClient(userID)
            let respBalance = await this.getCoinBalance(userID, inPair, binanceClient)
            if(respBalance.success === 0 ) throw respBalance.message
            let protectedAltcoin = await this.getProtected(userID, inPair)
            console.log("el balance protegido es ", protectedAltcoin)
            let quantityToSell = parseFloat(respBalance.balance) - parseFloat(protectedAltcoin)
            console.log("de valace", quantityToSell)
            quantityToSell = fixedToPattern(quantityToSell, lotSize)
            console.log("de vaquantityToSelllace", quantityToSell)
            let response = await binanceClient.order({
                type: "MARKET", symbol, side: "SELL", quantity: quantityToSell,
            })
            console.log("respuesta de mercado", response)
            return {success: 1}
        } catch (error) {
            return { success: 0, message: error.message ? error.message : error }
        }
        
    }
    async verifyDust(userID){
        try {
            const binanceClient = await this.getAlternativeClient(userID)
            let response = await binanceClient.dustLog()
            console.log("respuesta de verificar polvo", response)
            return {success: 1}
        } catch (error) {
            return { success: 0, message: error.message ? error.message : error }
        }
    }
    async symbolOpenOrders(userID, symbol, side){
        try {
            const binanceClient = await this.getClient(userID)
            let response = await binanceClient.openOrders({ symbol })
            //console.log("respuesta de ordenes abiertas del symbol", response)
            let filtrdOrdrs = side ? response.filter(val=> val.side === side) : response
            //console.log("respuesta de filtradas", filtrdOrdrs)
            return {success: 1, numberOfOpenOrders: filtrdOrdrs.length, filtrdOrdrs}
        } catch (error) {
            return { success: 0, message: error.message ? error.message : error }
        }
    }
    async cancelOrders(userID, symbol, respOpenOrders){
        try {
            const binanceClient = await this.getClient(userID)
            console.log("vamosa cancelar ", userID, symbol, respOpenOrders)
            for (let index = 0; index < respOpenOrders.length; index++) {
                const {clientOrderId, type} = respOpenOrders[index];
                if(type != "STOP_LOSS_LIMIT"){
                    console.log("si pasa a cancelar")
                    await binanceClient.cancelOrder({
                        symbol, origClientOrderId: clientOrderId
                    })
                }
            }
            return {success: 1}
        } catch (error) {
            console.log("hubo un error al cancelar orden", error)
            return { success: 0, message: error.message ? error.message : error }
        }
    }
    async regenerateOCO(userID, quantity, symbol, _gain, _sl, lotSize){
        try {
            console.log("llego a regenerar oco con", userID, quantity, symbol, _gain, _sl, lotSize)
            const binanceClient = await this.getClient(userID)
            let quantityToSell = fixedToPattern(quantity, parseFloat(lotSize))
            console.log("la cantidad a vender", quantityToSell)
            let cleanSL = this.convertExponentialToDecimal(_sl)
            let cleanGain = this.convertExponentialToDecimal(_gain)
            let respNewOCO = await binanceClient.orderOco({ symbol, side:  "SELL", quantity: quantityToSell, price: cleanGain, stopPrice: cleanSL, stopLimitPrice: cleanSL, 'recvWindow': 50000 })
            return { success: 1, orderID: respNewOCO.orders[0].clientOrderId }
        } catch (error) {
            console.log("hub", error)
            return { success: 0, message: error.message ? error.message : error }
        }
    }
    convertExponentialToDecimal(exponentialNumber) {
        // sanity check - is it exponential number
        const str = exponentialNumber.toString();
        if (str.indexOf('e') !== -1) {
            const result = exponentialNumber.toFixed(8);
            return result;
        } else {
            return exponentialNumber;
        }
    }
    async regenerateQuantities(account, symbol, initialOrderID){
        try {
            const binanceClient = await this.getClient(account)
            let buyOrders = []
            let sellOrders = []
            let neutralOrders = []
            let totalBuyed = 0
            let totalSelled = 0
            let orddrs = await binanceClient.allOrders({
                symbol,
                orderId: initialOrderID,
                recvWindow: 50000
            })
            //console.log("los ytaes son", orddrs)
            for (let index = 0; index < orddrs.length; index++) {
                const element = orddrs[index];
                if(element.cummulativeQuoteQty && !isNaN(element.cummulativeQuoteQty) && element.cummulativeQuoteQty > 0 ){
                    if(element.side === "BUY"){
                        totalBuyed += element.cummulativeQuoteQty
                        buyOrders.push(element)
                    } else {
                        totalSelled += element.cummulativeQuoteQty
                        sellOrders.push(element)
                    }
                } else {
                    neutralOrders.push(element)
                }
            }
            console.log("buyOrders", buyOrders, "sellOrders", sellOrders, "neutralOrders", neutralOrders, totalBuyed, totalSelled)
            let change = totalSelled - totalBuyed
            let percentChage = ( change * 100 ) / totalBuyed
            console.log("calculos", change, percentChage)
            return {success: 1, buyOrders, sellOrders, neutralOrders, totalBuyed, totalSelled , change : change.toFixed(8), percentChage: percentChage.toFixed(2) }
        } catch (error) {
            console.log("el error fue", error)
            return { success: 0, message: error.message ? error.message : error }
        }
    }
}

module.exports = new Exchange()