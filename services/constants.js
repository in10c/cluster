const bitcoin = require('bitcoinjs-lib')

const Constants = {
    production: false,
    enterPercentage: .08,
    bronzeEnterPercentage: .15,
    maxOcupancy: 30,
    packgs : {
        1: {name:"Bronce", limits: 0.042 }, 
        2: {name: "Plata", limits: 0.084 }, 
        3: {name:"Oro", limits: 0.212 }, 
        4: {name:"Diamante", limits: 0.509 }, 
        5: {name:"Premium", limits: 1.018 }, 
        6: {name:"Premium Plus", limits: 2.036 }
    },
    mainServer: ()=>{
        return Constants.production ? 'https://bitwabi.com:3005' : 'http://localhost:3005'
    },
    databaseStringConn: ()=>{
        if(Constants.production){
            return 'mongodb://mongoAdmin:Vudh8Li1rzxPxQ7DnkMLS33@82.223.104.116:27017/bitwabi?authSource=admin'
        } else {
            return 'mongodb://localhost:27017/bitwabiprod'
        }
    },
    appUrl: ()=>{
        if(Constants.production){
            return 'https://app.bitwabi.com'
        } else {
            return 'http://localhost:3000'
        }
    },
}

module.exports = Constants