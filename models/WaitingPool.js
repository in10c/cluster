var mongoose =  require('mongoose');
var Schema = mongoose.Schema;

var WaitingSchema = Schema({
    userID: String
});

module.exports = mongoose.model('WaitingPool', WaitingSchema);