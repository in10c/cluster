var mongoose =  require('mongoose');
var Schema = mongoose.Schema;

var DustSchema = Schema({
    time: Date,
    bnb: Number
}, {timestamps: true});

module.exports = mongoose.model('Dust', DustSchema);