var mongoose =  require('mongoose');
var Schema = mongoose.Schema;

var SlotSchema = Schema({
    userID: String,
    clusterNumber: Number,
    socketID: String
}, {timestamps: true});

module.exports = mongoose.model('Slot', SlotSchema);