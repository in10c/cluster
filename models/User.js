var mongoose =  require('mongoose');
var Schema = mongoose.Schema;

var TokenSchema = Schema({
    name: String,
    lastName: String,
    nickName: String,
    email: String,
    password: String,
    phone: String, 
    netPaymentsWallet: String, 
    selectedPackage: Number,
    reservation: Boolean,
    active: Boolean,
    downgrade: Boolean,
    fullAccount: Boolean,
    trader: Boolean,
    developer: Boolean,
    verifiedphone: Boolean,
    termsAndConditionsAgreement: Boolean,
    termsAndConditionsAgreementDate: Date
}, {timestamps: true});

module.exports = mongoose.model('Users', TokenSchema);