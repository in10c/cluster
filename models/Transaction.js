var mongoose =  require('mongoose');
var Schema = mongoose.Schema;

var Operation = Schema({
    description: String,
    balance: Number,
    affecting: Number,
    time: Date,
    userID: String
})

module.exports = mongoose.model('Transaction', Operation);