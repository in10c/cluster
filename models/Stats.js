var mongoose =  require('mongoose');
var Schema = mongoose.Schema;

var StatsSchema = Schema({
    userID: String,
    balance: Number,
    botStatus: Boolean
});

module.exports = mongoose.model('Stat', StatsSchema);