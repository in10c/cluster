var child_process = require("child_process");
const Constants = require("../services/constants")
const Slot = require("../models/Slot")
let childProceses = {}

class Cluster {
    newClients(usersList){
        //console.log("llegamos a new clientes", usersList)
        usersList.map((val)=>{
            this.createOne(val.userID)
        })
        //console.log("asi quedo child", childProceses)
    }
    createOne( userID ) {
        childProceses[userID] = {}
        //comunicacion con hijo
        childProceses[userID]["process"] = child_process.fork("./vps/VPS", [JSON.stringify({ userID, gClusterNumber })])
        childProceses[userID]["logs"] = []
        //childProceses[userID]["order"] = order
        childProceses[userID]["process"].on("message", msg=>{
            let objt = JSON.parse(msg)
            childProceses[userID]["logs"].push(objt)
        })
        //guardado en listado de bots
        childProceses[userID]["status"] = "Bot creado"
    }
    async getOcupation(){
        let slots = await Slot.find({clusterNumber: gClusterNumber})
        let spacesUsed = slots.length
        //let spacesUsed = Object.keys(childProceses).length
        let freeSpace = Constants.maxOcupancy - spacesUsed
        return {
            spacesUsed, freeSpace, clusterNumber: gClusterNumber
        }
    }
    returnRobots(){
        let returningArray = []
        Object.keys(childProceses).map(userID=>{
            returningArray.push({
                userID,
                status: childProceses[userID].status,
                logs: childProceses[userID].logs
            })
        })  
        return returningArray
    }
    async reBorn(){
        let slots = await Slot.find({clusterNumber: gClusterNumber})
        console.log("reborning", slots)
        for (let index = 0; index < slots.length; index++) {
            const element = slots[index];
            child_process.fork("./vps/VPS", [JSON.stringify({ userID: element.userID, gClusterNumber, reBorn: true })])
        }
    }
}

module.exports = new Cluster()