const Exchange = require("../services/exchange")
const Signal = require("../models/Signal")
const OrdersQueue = require("../models/OrdersQueue")
const Order = require("../models/Order")
const {sendTelegramMessage} = require("../services/telegram")
const Slot = require("../models/Slot")
const Constants = require("../services/constants")

class Robot {

    async createBuyOrder(userID, orderID, symbol, price, lotSize, pretendedToBuy){
        console.log("nao dn ereacrobot ")
        const response = await Exchange.createBuyOrder(userID, symbol, price, lotSize, pretendedToBuy)
        if(response.success===0){
            console.log("vamos a actualizar order queue", orderID)
            await this.changeOrderStatus(orderID, "Error al crear la orden de compra: "+response.message ? response.message : "")
        } else {
            const {newOrder, buyBalance} = response
            await OrdersQueue.findByIdAndUpdate(orderID, {
                buyOrderID: newOrder.clientOrderId, buyedQuantity: buyBalance, initialOrderID : newOrder.orderId,
                buyOrderQuantity: newOrder.origQty, buyOrderStatus: newOrder.status,
                buyOrderExecutionTime: newOrder.transactTime, buyOrderPrice: newOrder.price,
            })
            await this.changeOrderStatus(orderID, "Éxito al crear la orden de compra")
        }
        return response
    }
    async moveToHistoric(orderID){
        let orderQueued = await OrdersQueue.findById(orderID)
        orderQueued = JSON.parse(JSON.stringify(orderQueued))
        if(orderQueued.buyOrderID && orderQueued.sellOrderID){
            let response = await Exchange.regenerateQuantities(orderQueued.userID, orderQueued.symbol, orderQueued.initialOrderID)
            orderQueued.spendedQuantity= response.totalBuyed
            orderQueued.receivedQuantity= response.totalSelled
            if(
                response.totalBuyed && response.totalBuyed > 0 &&
                response.totalSelled && response.totalSelled > 0 &&
                response.change && !isNaN(response.change) &&
                response.percentChage && !isNaN(response.percentChage)){
                    orderQueued.resultantQuantity= response.change
                    orderQueued.resultantPercentage= response.percentChage
            }
            orderQueued.stats= {
                buyOrders: response.buyOrders,
                sellOrders: response.sellOrders,
                neutralOrders: response.neutralOrders,
                totalBuyed: response.totalBuyed,
                totalSelled: response.totalSelled,
            }
        }
        
        delete orderQueued._id
        delete orderQueued.__v
        let hOrder = new Order(orderQueued)
        await hOrder.save()
        await OrdersQueue.findByIdAndDelete(orderID)
    }
    async getBinanceClient(userID){
        let exchangeClient = await Exchange.getClient(userID)
        return exchangeClient
    }
    async verifyDust(userID){
        let exchangeClient = await Exchange.verifyDust(userID)
    }
    async changeOrderStatus(orderID, newStatus){
        let today = new Date()
        let currentOrder = await OrdersQueue.findById(orderID)
        currentOrder.globalStatus = newStatus
        if(!currentOrder.logs){
            currentOrder.logs = []
        }
        currentOrder.logs.push({
            description: newStatus,
            time: today.getTime()
        })
        await currentOrder.save()
    }
    async updateOrder(orderID, data){
        await OrdersQueue.findByIdAndUpdate(orderID, data)
    }
    async editOCO(userID, orderID, sl, gain){
        const { symbol, lotSize, tradedPair, sellOrderQuantity } = await OrdersQueue.findById(orderID)
        console.log("camos a crear nueva orden")
        let response = await Exchange.newOCO(userID, tradedPair, lotSize, symbol, sl, gain, sellOrderQuantity)
        //await this.changeOrderStatus(orderID, response.success===0 ? "Error al editar la orden de venta: "+response.message: "Éxito al editar la orden de venta")
        if(response.success===1){
            await OrdersQueue.findByIdAndUpdate(orderID, { sl, gain })
        }
        return response
    }
    async createOCO(userID, orderID){
        const { sl, gain, symbol, lotSize, tradedPair, sellOrderID } = await OrdersQueue.findById(orderID)
        let response = await Exchange.newOCO(userID, tradedPair, lotSize, symbol, sl, gain)
        if(response.success===0){
            await this.sendTgmMessage("Error al crear la orden de venta del par "+symbol+" al usuario "+userID+" porque : "+response.message, userID)
            await this.changeOrderStatus(orderID, "Error al crear la orden de venta: "+response.message)
        } else {
            await this.changeOrderStatus(orderID, "Éxito al crear la orden de venta")
        }   
        
        return response
    }
    async regenerateOCO( userID, orderID, quantity, sl, gain, symbol, lotSize ){
        let response = await Exchange.regenerateOCO(userID, quantity, symbol, gain, sl, lotSize)
        await this.changeOrderStatus(orderID, response.success===0 ? "Error al re-generar la orden de venta: "+response.message: "Éxito al re-generar la orden de venta")
        return response
    }
    async updateOrderRegistry(orderID, newStatus){
        console.log("vamos a cambiar update a ", orderID, newStatus)
        await this.changeOrderStatus(orderID, newStatus)
        return newStatus
    }
    async updateBuyOrderStatus(orderID, msg){
        //reaccionamos dependiendo el estado
        if(msg.orderStatus === "NEW"){
            //si es nueva solo actuyalizamos estado
            await this.updateOrder(orderID, { buyOrderStatus: msg.orderStatus })
            return await this.updateOrderRegistry(orderID, "Orden de compra creada")
        } else if(msg.orderStatus === "PARTIALLY_FILLED" || msg.orderStatus === "FILLED"){
            //actualizamos estados
            let partial = msg.orderStatus === "PARTIALLY_FILLED" 
            await this.updateOrder(orderID, { buyOrderStatus: msg.orderStatus })
            return await this.updateOrderRegistry(orderID, "Orden de compra "+(partial ? "parcialmente" : "" )+" ejecutada" )
        //para los otros estados
        } else if (msg.orderStatus === "CANCELED") {
            await this.updateOrder(orderID, { buyOrderStatus: msg.orderStatus})
            return await this.updateOrderRegistry(orderID, "La orden de compra ha sido cancelada" )
        } else {
            await this.updateOrder(orderID, { buyOrderStatus: msg.orderStatus})
            return await this.updateOrderRegistry(orderID, "La orden de compra tiene un estado sin acción." )
        }
    }
    async updateSellOrderStatus(orderID, msg){
        if(msg.orderStatus === "NEW" && msg.orderType === "LIMIT_MAKER"){
            //registramos estado de la venta nueva
            await this.updateOrder(orderID, {
                sellOrderID: msg.newClientOrderId, sellOrderQuantity: msg.quantity,
                sellOrderPrice: msg.price, sellOrderExecution: msg.creationTime, sellOrderStatus: msg.orderStatus
            })
            return await this.updateOrderRegistry(orderID, "Orden de venta creada" )
        //si una orden de compra ya se lleno o parcial
        } else if(msg.orderStatus === "PARTIALLY_FILLED" || msg.orderStatus === "FILLED"){
            let partial = msg.orderStatus === "PARTIALLY_FILLED" 
            await this.updateOrder(orderID, { 
                sellOrderStatus: msg.orderStatus, selledQuantity: msg.totalTradeQuantity, sellOrderType: msg.orderType
            })
            return await this.updateOrderRegistry(orderID, "Orden de venta "+(partial ? "parcialmente" : "" )+" ejecutada" )
        //para los otros estados
        } else if (msg.orderStatus === "CANCELED") {
            await this.updateOrder(orderID, { sellOrderStatus: msg.orderStatus})
            return await this.updateOrderRegistry(orderID, "La orden de venta ha sido cancelada" )
        //si es expired o otra cosa rara no tiene accion
        } else {
            await this.updateOrder(orderID, { sellOrderStatus: msg.orderStatus})
            return await this.updateOrderRegistry(orderID, "La orden de venta ("+msg.orderType+") tiene un estado ("+msg.orderStatus+") sin acción.")
        }
    }
    async editBuyOrder(orderID, userID, enterprice, symbol, toBuyAmout, lotSize){
        const responseNew = await Exchange.editBuyOrder(userID, symbol, toBuyAmout, enterprice, lotSize)
        if(responseNew.success === 1){
            await OrdersQueue.findByIdAndUpdate(orderID, {
                enterprice: enterprice
            })
            await this.changeOrderStatus(orderID, "Se ha editado la orden de compra correctamente, el nuevo ID de la orden es: "+responseNew.order.clientOrderId)
        } else {
            await this.changeOrderStatus(orderID, "Error al editar orden de compra: "+responseNew.message)
        }
    }
    async deleteAndSell(userID, orderID, auxOrder){
        const {sellOrderID, symbol, tradedPair, lotSize} = auxOrder
        console.log("entro en delete an sell con ", sellOrderID, symbol, tradedPair, lotSize)
        if( sellOrderID ){
            let sellResp = await Exchange.panicSell(userID, tradedPair, lotSize, symbol)
            return sellResp
        }
    }
    async getQueuedOrderInfo(orderID){
        let response = await OrdersQueue.findById(orderID)
        return response
    }
    async deleteSlot(userID, gClusterNumber){
        await Slot.findOneAndRemove({userID, clusterNumber: gClusterNumber})
    }
    async updateSignal(signalID, data){
        await Signal.findByIdAndUpdate(signalID, data)
    }
    async symbolOpenOrders(userID, symbol, side){
        let response = await Exchange.symbolOpenOrders(userID, symbol, side)
        return response
    }
    async cancelOrders(userID, symbol, respOpenOrders){
        await Exchange.cancelOrders(userID, symbol, respOpenOrders)
    }
    async getOpenOrder(userID, symbol){
        return await OrdersQueue.findOne({userID, symbol})
    }
    async sendTgmMessage(msg, userID){
        let auxLink = Constants.appUrl()+"/account/"+userID
        msg += " <a href='"+auxLink+"'>Revisar cliente</a>"
        await sendTelegramMessage(msg, auxLink)
    }
}

module.exports = new Robot()