const Order = require("../models/Order")
const OrdersQueue = require("../models/OrdersQueue")

class Orders {
    async getActiveOrders ( userID ) {
        let orders = await OrdersQueue.find({ userID })
        return orders
    }
    async getSignalOrders( signalID ){
        let orders = await OrdersQueue.find({ signalID })
        return orders
    }
    async nonBuyedOrders(userID){
        let orders = await OrdersQueue.find({ userID, globalStatus: "Proceso iniciado" })
        return orders
    }
    async nonSelledOrders(userID){
        let orders = await OrdersQueue.find({ 
            userID, 
            buyOrderID: { $exists : true}, 
            buyOrderStatus : { $in: ["FILLED", "PARTIALLY_FILLED"]},
            sellOrderID: { $exists : false}
        })
        return orders
    }
}

module.exports = new Orders()