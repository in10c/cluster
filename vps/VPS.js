//addons import
var mongoose = require('mongoose');
var debounce = require('debounce');
//controllers and services import
const Constants = require("../services/constants")
const Orders = require("../controllers/Orders")
const Robot = require("../controllers/Robot")
// socket io
const io = require('socket.io-client');
//real time alert server socket 
const socket = io(Constants.mainServer(), {secure: true, reconnect: true, rejectUnauthorized : false});
//global variables
const { userID, gClusterNumber, reBorn } = JSON.parse(process.argv[2])

console.log("Inicie nuevo bot para => ", userID)
//communicacion con el sevridor central
socket.on('connect', () => {
    console.log("El nuevo socket id => ",socket.id); 
    // socket.on("editBuyOrder", data=> editBuyOrder(data.symbol, data.enterprice) )
    // socket.on("kill", data=> deleteOrder(data.symbol) )
    // socket.on("editOCO", data=> editOCO(data.sl, data.tp, data.symbol) )
    // socket.on("refreshOrders", async () => {
    //     console.log("vamos a refrescar las ordenes")
    //     //await getOrderEstructure(false)
    //     await searchOrdersWOBuyOrder()
    //     await searchOrdersWOSellOrder()
    // } )
    socket.on("deleteVPS", async ()=> {
        console.log("vamos a matar el vps")
        await Robot.deleteSlot(userID, gClusterNumber)
        process.exit(1)
    })
    socket.on("regenerateSellOrder", async (data)=> {
        console.log("vamos a regenerar orden venta", data)
        const { quantity, asset } = data
        let symbol = asset+"BTC"
        let auxOrder = await Robot.getOpenOrder(userID, symbol)
        if(!auxOrder) {
            Robot.sendTgmMessage("No se encontró orden activa para símbolo "+symbol+" en el usuario "+userID+".", userID)
            return false
        }
        const { _id : orderID, sl, gain, lotSize } = auxOrder
        let response = await Robot.regenerateOCO( userID, orderID, quantity, sl, gain, symbol, lotSize )
        console.log("respuesta de regenerar oco", response)

    })
});

socket.on("reconnect", async ()=>{
    socket.emit("saveSlot", {userID, socketID: socket.id, clusterNumber: gClusterNumber, reBorn})
})

//funcion para conectarme a la base
const connectDatabase = (cb) => {
    mongoose.Promise = global.Promise;
    mongoose.connect(Constants.databaseStringConn(), { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false})
    .then(() => cb() ).catch(err => console.log(err));
}

const searchOrdersWOBuyOrder = async () =>{
    let wobuyorder = await Orders.nonBuyedOrders(userID)
    console.log("wobuyorder", wobuyorder)
    //primero ejecutamos los que no tienen orden
    wobuyorder.map(async item => await Robot.createBuyOrder(userID, item._id, item.symbol, item.enterprice, item.lotSize, item.pretendedToBuy))
}

const searchOrdersWOSellOrder = async () =>{
    let wosellorder = await Orders.nonSelledOrders(userID)
    console.log("wobuyorder", wosellorder)
    //primero ejecutamos los que no tienen orden
    wosellorder.map(async item => await Robot.createOCO( userID, item._id ) )
}
 
// const deleteOrder = async(symbol)=> {
//     let auxOrder = await Robot.getOpenOrder(userID, symbol)
//     if(!auxOrder) {
//         console.log("no se encontro orden activa para simbolo", symbol, userID)
//         return false
//     }
//     const { _id : orderID, signalID } = auxOrder
//     let respOpenOrders = await Robot.symbolOpenOrders(userID, symbol)
//     if(respOpenOrders.success === 0 ) {
//         await updateOrderLogs(orderID, "Error al traer numero de ordenes abiertas del symbol("+symbol+"): "+respOpenOrders.message, symbol)
//         return true
//     } else {
//         await Robot.cancelOrders(userID, symbol, respOpenOrders.filtrdOrdrs)
//         //timeout necesario para que de nuevo el balance sea el correcto despues de cancelar ordenes
//         setTimeout( async ()=>{
//             let response = await Robot.deleteAndSell(userID, orderID, auxOrder)
//             console.log("regreso a vps la resp", response)
//             //await killThis(orderID, symbol, "MARKET", signalID)
//         }, 2000)
        
//     }
// }
// const editOCO = async(sl, tp, symbol) => {
//     //primero requerimos las ordenes abiertas
//     const { _id : orderID, lotSize } = await Robot.getOpenOrder(userID, symbol)
//     let fail = await deleteSideOrdersIfExist(orderID, symbol, "SELL")
//     if(fail) return false
//     let respNewOCO = await Robot.editOCO(userID, orderID, sl, tp)
//     let {success, message} = respNewOCO
//     if( success === 1 ){
//         await updateOrderLogs(orderID, "Se ha editado la orden de venta correctamente, el nuevo ID de la orden es: "+respNewOCO.orderID, symbol)
//     } else {
//         await updateOrderLogs(orderID, "Error al editar orden de venta: "+message, symbol)
//     }
// }
const deleteSideOrdersIfExist = async (orderID, symbol, side) => {
    let respOpenOrders = await Robot.symbolOpenOrders(userID, symbol, side)
    if(respOpenOrders.success === 0 ) {
        await updateOrderLogs(orderID, "Error al traer numero de ordenes abiertas del symbol("+symbol+"): "+respOpenOrders.message, symbol)
        return true
    } else {
        if(respOpenOrders.numberOfOpenOrders > 0){
            console.log("como tiene ordenes abiertas procedemos a cancelar")
            //vamos a eliminar las ordenes de venta
            await Robot.cancelOrders(userID, symbol, respOpenOrders.filtrdOrdrs)
            return false
        } else {
            return false
        }
    }
}
// const deleteBuyOrdersIfExist = async (orderID, symbol) => {
//     let respOpenOrders = await Robot.symbolOpenOrders(userID, symbol, "BUY")
//     if(respOpenOrders.success === 0 ) {
//         await updateOrderLogs(orderID, "Error al traer numero de ordenes abiertas del symbol("+symbol+"): "+respOpenOrders.message, symbol)
//         return {fail: true}
//     } else {
//         if(respOpenOrders.numberOfOpenOrders > 0){
//             //vamos a eliminar las ordenes de venta
//             let toBuyAmout = 0 
//             for (let index = 0; index < respOpenOrders.filtrdOrdrs.length; index++) {
//                 const {origQty, executedQty} = respOpenOrders.filtrdOrdrs[index];
//                 toBuyAmout += origQty - executedQty
//             }
//             await Robot.cancelOrders(userID, symbol, respOpenOrders.filtrdOrdrs)
//             console.log("voy a saltar estos skips: ", respOpenOrders.numberOfOpenOrders, respOpenOrders)
//             return {fail: false, toBuyAmout}
//         } else {
//             await updateOrderLogs(orderID, "No existen ordenes abiertas de compra para editar", symbol)
//             return {fail: true}
//         }
//     }
// }
// const editBuyOrder = async (_symbol, _enterprice) => {
//     console.log("vamos a editar la orden", _symbol, _enterprice) 
//     const { _id : orderID, lotSize } = await Robot.getOpenOrder(userID, _symbol)
//     let response = await deleteBuyOrdersIfExist(orderID, _symbol)
//     if(response.fail) return false
//     await Robot.editBuyOrder(orderID, userID, _enterprice, _symbol, response.toBuyAmout, lotSize)
// }
const updateOrderLogs = async (orderID, newStatus, symbol) => {
    await Robot.changeOrderStatus(orderID, newStatus)
}
const createSellOrder = async (orderID, symbol) => {
    //envio informacion por si ya tenia orden de venta
    let fail = await deleteSideOrdersIfExist(orderID, symbol, "SELL")
    if(fail) return false
    await Robot.createOCO( userID, orderID )
}
//auxiliares
const statusThatCreateOCO = ["PARTIALLY_FILLED", "FILLED"]
const websocketMsg = async (msg) => {
    //si el evento es diferente a ejecucion o aun no esta el puntero o no existe no nos interesa ejecutar
    const openOrder = await Robot.getOpenOrder(userID, msg.symbol)
    if(msg.eventType === "executionReport" && openOrder) {
        console.log(">>>>>>>>> Han llegado cambios desde el socket del usuario: ", msg)
        const orderID = openOrder._id
        if( msg.side === "BUY"){
            await Robot.updateBuyOrderStatus(orderID, msg)
            if(statusThatCreateOCO.includes(msg.orderStatus)){
                debounce(createSellOrder(orderID, msg.symbol), 3000)
            }
        } else if ( msg.side === "SELL") {
            await Robot.updateSellOrderStatus(orderID, msg)
            if(msg.orderStatus === "FILLED"){
                killThis(orderID, msg.symbol, msg.orderType, openOrder.signalID)
            }
        }
    }
}
//cuando ya termina las ordenes deja de ejecutarse
const killThis = async (orderID, _symbol, orderType, signalID) => {
    console.log("vamos a mover a historico.... ")
    await Robot.moveToHistoric(orderID)
    let ordersResponse = await Orders.getActiveOrders(userID)
    let signalOrders = await Orders.getSignalOrders(signalID)
    //si de la señal no hay mas ordenes y esta se eliminara se cambia el estado de la señal
    console.log("revisaremos si matamos la señal", signalID, signalOrders.length, )
    if(signalOrders.length === 0){
        console.log("si paso a matar la senal")
        await Robot.updateSignal(signalID, { active: false, kindOfClose: orderType })
    }
    //si no hay mas ordenes abiertas del cliente se elimina el vps
    if(ordersResponse.length === 0){
        await Robot.deleteSlot(userID, gClusterNumber)
        process.exit(1)
    }
}

const healthCheck = async () => {
    let ordersResponse = await Orders.getActiveOrders(userID)
    if (ordersResponse.length === 0 ) {
        await Robot.sendTgmMessage("El usuario "+userID+" tiene un VPS activo pero no tiene ordenes abiertas en Bitwabi.", userID)
        return false
    } 
    //console.log("las ordenes abiertas son ", ordersResponse)
    for (let index = 0; index < ordersResponse.length; index++) {
        const {symbol} = ordersResponse[index];
        let respOpenOrders = await Robot.symbolOpenOrders(userID, symbol)
        //console.log("la respuesta de ordenes abiertas es", respOpenOrders)
        if(respOpenOrders.success=== 0) {
            await Robot.sendTgmMessage("Se ha fallado al traer ordenes abiertas en el chequeo de salud, par "+symbol+" del usuario "+userID+".", userID)
        } else {
            if(respOpenOrders.numberOfOpenOrders <= 0 ){
                await Robot.sendTgmMessage("El usuario "+userID+" tiene ordenes abiertas en bitwabi pero no en el exchange con el par "+symbol+".", userID)
            }
        }
    }
}

//iniciamos
connectDatabase(async ()=>{
    //iniciamos websocket
    const binanceClient = await Robot.getBinanceClient(userID)
    binanceClient.ws.user(websocketMsg, () => {
        if(!reBorn) searchOrdersWOBuyOrder()
        if(reBorn) searchOrdersWOSellOrder()
        //cada 15 minutos 15 * 60000 
        setInterval(healthCheck, 900000)
    })
    //el socket emite guardado se slot 
    socket.emit("saveSlot", {userID, socketID: socket.id, clusterNumber: gClusterNumber, reBorn})
})
