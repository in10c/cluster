const fs = require('fs');
var mongoose = require('mongoose');
global.gClusterNumber = 2
var app = require('./app');
const Constants = require("./services/constants")

var Server;
if(false){
    let options = {
        key: fs.readFileSync(__dirname + '/_.bitwabi.com_private_key.key', 'utf8'),
        cert: fs.readFileSync(__dirname + '/bitwabi.com_ssl_certificate.cer', 'utf8')
    };
    Server = require('https').createServer(options, app); 
} else {
    Server = require('http').createServer(app);
}   

var port = 3020;
// Le indicamos a Mongoose que haremos la conexión con Promesas
mongoose.Promise = global.Promise;
// Usamos el método connect para conectarnos a nuestra base de datos
mongoose.connect(Constants.databaseStringConn(), { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false})
.then(() => {

    Server.listen(port, () => {
        console.log("servidor corriendo en http://localhost:"+port);
    });
})
.catch(err => console.log(err));