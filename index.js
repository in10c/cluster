const fs = require('fs');
var mongoose = require('mongoose');
global.gClusterNumber = 1 
var app = require('./app');
const Constants = require("./services/constants")

var Server;
if(Constants.production){
    let options = {
        key: fs.readFileSync(__dirname + '/_.bitwabi.com_private_key.key', 'utf8'),
        cert: fs.readFileSync(__dirname + '/bitwabi.com_ssl_certificate.cer', 'utf8'),
        ca: fs.readFileSync(__dirname + '/_.bitwabi.com_ssl_certificate_INTERMEDIATE.cer', 'utf8')
    };
    Server = require('https').createServer(options, app); 
} else {
    Server = require('http').createServer(app);
}   

var port = 3010;
// Le indicamos a Mongoose que haremos la conexión con Promesas
mongoose.Promise = global.Promise;
// Usamos el método connect para conectarnos a nuestra base de datos
mongoose.connect(Constants.databaseStringConn(), { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false})
.then(() => {

    Server.listen(port, () => {
        const Cluster = require("./controllers/Cluster")
        Cluster.reBorn()
        console.log("servidor corriendo en http://localhost:"+port);
    });
})
.catch(err => console.log(err));