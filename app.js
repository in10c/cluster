var express = require('express');
var bodyParser = require('body-parser')
var cors = require('cors')

const Cluster = require("./controllers/Cluster")
const { verifyAuthToken } = require('./services/jsonWebToken')

var app = express();
var jsonParser = bodyParser.json()
app.use(cors())

app.get('/getOccupied', async function (req, res) {
    let disponibility = await Cluster.getOcupation()
    res.send({success: 1, disponibility})
}) 

app.post('/generateVPS', jsonParser, async function (req, res) {
    const { usersList } = req.body
    await Cluster.newClients(usersList)
    res.send({success: 1})
}) 

module.exports = app